<?php
define('SITE_DEFAULT_TEMPLATE_PATH', '/local/templates/.default/');

function debug($data) {
  echo '<div><pre>';
  print_r($data);
  echo '</pre></div>';
}

// Вырезает из строки строку нужной длины
function cutStr($str, $length = 50, $postfix = '...') {
  if (strlen($str) <= $length)
    return $str;

  $temp = substr($str, 0, $length);
  return rtrim(substr($temp, 0, strrpos($temp, ' ')), ',') . $postfix;
}