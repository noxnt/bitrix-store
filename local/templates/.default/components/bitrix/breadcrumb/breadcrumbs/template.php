<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

 	  // <ul class="breadcrumbs">
    //   <li class="home">
    //      <a href="index.html" title="Go to Home Page"><img src="images/home.png" alt=""/></a>&nbsp;<span>&gt;</span>
    //   </li>
    //   <li>Dresses<span>&gt;</span></li>
    //   <li><span class="red">&nbsp;Clothes&nbsp;</span></li>
    // </ul>
    // <ul class="previous">
    // 	<li><a href="index.html">Back to Previous Page</a></li>
    // </ul>

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '<div class="dreamcrub">
								<ul class="breadcrumbs">
									<li class="home">
    	      				<a href="/" title="Go to Home Page"><i class="fa fa-home"></i></a><span>&nbsp;&gt;</span>
       						</li>';

//we can't use $APPLICATION->SetAdditionalCSS() here because we are inside the buffered function GetNavChain()
$css = $APPLICATION->GetCSSArray();
if(!is_array($css) || !in_array("/bitrix/css/main/font-awesome.css", $css))
{
	$strReturn .= '<link href="'.CUtil::GetAdditionalFileURL("/bitrix/css/main/font-awesome.css").'" type="text/css" rel="stylesheet" />'."\n";
}

$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
	$arrow = ($index > 0? '&nbsp;<i class="fa fa-angle-right"></i>&nbsp;' : '');

	if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
	{
		$strReturn .= '
			<li>
				'.$arrow.'
				<a href="'.$arResult[$index]["LINK"].'" title="'.$title.'">
					<span itemprop="name">'.$title.'</span>
				</a>
				<meta itemprop="position" content="'.($index + 1).'" />
			</li>';
	}
	else
	{
		$strReturn .= '
			<li>
				'.$arrow.'
				<span class="red">'.$title.'</span>
			</li>';
	}
}

$strReturn .= '</ul><div class="clearfix"></div></div>';

return $strReturn;